<?php
require_once 'classes/Entries.php';
require_once 'config/db.php';

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Article</title>
</head>
<body>
<form action="storeEntire.php" method="post">
    <div>
        <label>Title: <input type="text" name="title"></label>
    </div>
    <div>
        <label>Intro: <input type="text" name="intro"></label>
    </div>
    <div>
        <label>Content: <textarea name="content"></textarea></label>
    </div>
    <button>Save</button>
</form>

</body>
</html>