<?php
require_once 'config/db.php';
require_once 'classes/Entries.php';
$entries = Entries::all($pdo);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div>
    <ul>
        <li>
            <a href="createBlogTable.php">Create Table</a>
        </li>
        <li>
            <a href="addBlogArticle.php">Add new Article</a>
        </li>
    </ul>
</div>
<div>

    <?php foreach ($entries as $entrie):?>
        <?=$entrie->getId()?>
        <h1><?=$entrie->getTitle()?></h1>
        <p>Intro: <?=$entrie->getIntro()?></p>
        <p><?=$entrie->getContent()?></p>
        <a href="editEntrie.php?id=<?= $entrie->getId()?>">Edit Entrie</a>

        <form action="delete.php" method="post">
            <input type="hidden" name="entrie_id" value="<?=$entrie->getId()?>">
            <button>Delete</button>
        </form>

    <?php endforeach;?>
</div>
</body>
</html>
