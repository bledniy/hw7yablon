<?php


class Entries
{
    protected $id;
    protected $title;
    protected $intro;
    protected $content;
    protected $createdAt;


    public function __construct($title, $intro, $content)
    {
        $this->title = $title;
        $this->intro = $intro;
        $this->content = $content;
    }
    static public function getById($id, PDO $pdo){
        $id = htmlspecialchars($id);
        try {
            $sql = "SELECT * FROM entries WHERE id=:id";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id',$id);
            $statement->execute();
            $entriesArr = $statement->fetchAll();
            $entrieArr = $entriesArr[0];
            $entrieObj = new self($entrieArr['title'], $entrieArr['intro'], $entrieArr['content']);
            $entrieObj->setId($entrieArr['id']);
            $entrieObj->setCreatedAt($entrieArr['date_created']);
            return $entrieObj;

        }catch (Exception $exception){
            echo "Error getting entries! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }
    public function store(PDO $pdo){
        try {
            $sql = 'INSERT INTO entries SET 
            title = :title,
            intro = :intro,
            content = :content,
            date_created = CURDATE();
        ';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':title', $this->getTitle());
            $statement->bindValue(':intro', $this->getIntro());
            $statement->bindValue(':content', $this->getContent());
            $statement->execute();

        }catch (Exception $exception){
            echo "Error storing product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }
    static public function all($pdo){
        try {
            $sql = "SELECT * FROM entries";
            $pdoResult = $pdo->query($sql);
            $entriesArr = $pdoResult->fetchAll();
            $entriesObjs = [];
            foreach ($entriesArr as $entrieArr){
                $entriesObj = new self($entrieArr['title'],$entrieArr['intro'],$entrieArr['content']);
                $entriesObj->setId($entrieArr['id']);
                $entriesObj->setCreatedAt($entrieArr['date_created']);
                $entriesObjs[] = $entriesObj;
            }
            return $entriesObjs;

        }catch (Exception $exception){
            echo "Error getting products! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }

    }
    static public function upgrade($id,$title,$intro,$content, PDO $pdo ){
        $entrie = new self($title,$intro,$content);
        $entrie->setId($id);
        $entrie->update($pdo);

    }
    public function update(PDO $pdo){
        try {
            $sql = 'UPDATE entries SET
            title = :title,
            intro = :intro,
            content = :content
            WHERE id = :id';

            $statement = $pdo->prepare($sql);
            $statement ->execute([
                ':title' => $this->title,
                ':intro' => $this->intro,
                ':content' => $this->content,
                ':id'=> $this->id,
            ]);

        }catch (Exception $exception){
            echo "Error updating entrie! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();

        }

    }
    static public function delete($id,PDO $pdo){
        $entrie = self::getById($id,$pdo);
        $entrie ->destroy ($pdo);

    }
    public function destroy($pdo){
        try {
            $sql = 'DELETE FROM entries WHERE id = :id';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id',$this->id);
            $statement->execute();
        }catch (Exception $exception){
            echo "Error deleting entrie! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }

    }
    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getIntro()
    {
        return $this->intro;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }




}