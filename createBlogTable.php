<?php
require_once 'config/db.php';

try {
    $sqlEntries = "CREATE TABLE entries (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    intro VARCHAR(255),
    content TEXT,
    date_created DATE NOT NULL) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($sqlEntries);

    $sqlComments = "CREATE TABLE comments(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    body TEXT,
    entry_id INT,
    FOREIGN KEY (entry_id) REFERENCES entries(id)
    ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($sqlComments);

}catch (Exception $exception){
    echo "Error creating table! " . $exception->getCode() . ' message: ' . $exception->getMessage();
    die();
}