<?php

require_once 'classes/Entries.php';
require_once 'config/db.php';

if(empty($_GET['id'])){
    header('Location:index.php');
}
$entries = Entries::getById($_GET['id'],$pdo);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Article</title>
</head>
<body>
<form action="updateEntrie.php" method="post">
    <input type="hidden" name="entrie_id" value="<?=$entries->getId()?>">
    <div>
        <label>Title: <input type="text" name="title" value="<?=$entries->getTitle()?>"></label>
    </div>
    <div>
        <label>Intro: <input type="text" name="intro" value="<?=$entries->getIntro()?>"></label>
    </div>
    <div>
        <label>Content: <textarea name="content"> <?=$entries->getContent()?></textarea></label>
    </div>
    <button>Save</button>
</form>

</body>
</html>