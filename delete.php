<?php
if(empty($_POST['entrie_id'])){
    header('Location:index.php');
}

require_once 'config/db.php';
require_once 'classes/Entries.php';

Entries::delete($_POST['entrie_id'], $pdo);

header('Location:index.php');
